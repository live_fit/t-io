package org.tio.utils.date;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author tanyaowu 
 * 2018年6月10日 上午7:58:23
 */
public class DateUtils {
	
	/**
	 * 当前时间生成符合http响应头中的Date格式的字符串
	 * @return
	 * @author tanyaowu
	 */
	public static String httpDate() {
		return httpDate(new Date());
	}
	
	/**
	 * 把date生成符合http响应头中的Date格式的字符串
	 * @param date
	 * @return
	 * @author tanyaowu
	 */
	public static String httpDate(Date date) {
		SimpleDateFormat greenwichDate = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss 'GMT'", Locale.US);
		return greenwichDate.format(date);
	}
}
